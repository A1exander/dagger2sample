package com.example.daggersample.interfaces

/**
 * Абстракция, представляющая двигатель упрощенной "машины"
 *
 * Реализации: [com.example.daggersample.objects.engine.ElectricEngine], [com.example.daggersample.objects.engine.PetrolEngine]
 */
interface Engine {

    fun launch(): Boolean

    fun increaseGas()

    fun decreaseGas()

    fun switchOff(): Boolean

    fun isLaunched(): Boolean

}