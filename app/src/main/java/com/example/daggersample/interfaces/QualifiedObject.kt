package com.example.daggersample.interfaces

/**
 * Абстракция, представляющая объект, который будет определяться квалифаером
 *
 * Реализации: [com.example.daggersample.objects.qualifier_object.FirstQualifierObject],
 * [com.example.daggersample.objects.qualifier_object.SecondQualifierObject]
 */
interface QualifiedObject {

    fun someMethodHere()

}