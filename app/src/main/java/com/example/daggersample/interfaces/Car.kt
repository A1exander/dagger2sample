package com.example.daggersample.interfaces

/**
 * Абстракция, представляющая упрощенную "машину"
 *
 * Реализации: [com.example.daggersample.objects.car.ElectricCar], [com.example.daggersample.objects.car.PetrolCar]
 */
interface Car {

    fun launchEngine()

    fun switchOffEngine()

    fun increaseSpeed()

    fun decreaseSpeed()

    fun refuel()

    fun needToRefuel(): Boolean

    fun turnOnAudioSystem()

    fun turnOffAudioSystem()

    fun changeRadioStation()

    fun autoSelectRadioChannel()

}