package com.example.daggersample.interfaces

import com.example.daggersample.enums.TankStatus

/**
 * Абстракция, представляющая "бак" упрощенной машины
 *
 * Реализации: [com.example.daggersample.objects.tank.ElectricTank], [com.example.daggersample.objects.tank.PetrolTank]
 */
interface Tank {

    fun isEmpty(): Boolean

    fun fill()

    fun status(): TankStatus

    fun reduceFuel()

}