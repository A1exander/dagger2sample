package com.example.daggersample.enums

enum class TankStatus {
    EMPTY,
    HALF_FILLED,
    FULL
}