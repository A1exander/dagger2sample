package com.example.daggersample

import android.app.Application
import com.example.daggersample.di.components.DaggerMainComponent
import com.example.daggersample.di.components.MainComponent

class SampleApplication : Application() {

    companion object {
        lateinit var mainComponent: MainComponent
    }

    override fun onCreate() {
        super.onCreate()

        /**
         * Создаем инстанс главного компонента
         */
        mainComponent = DaggerMainComponent
            .factory()
            .create()
    }

}