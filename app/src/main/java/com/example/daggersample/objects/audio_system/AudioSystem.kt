package com.example.daggersample.objects.audio_system

import com.example.daggersample.utils.logMessage
import javax.inject.Inject
import kotlin.random.Random

/**
 * Объект аудиосистемы, создаваемый даггером
 */
class AudioSystem @Inject constructor() {

    private var currentChannel: Double = 101.2
    private var isTurnedOn: Boolean = false

    fun turnOn(isEngineLaunched: Boolean) {
        when {
            isEngineLaunched -> {
                if (!isTurnedOn) {
                    !isTurnedOn
                    logMessage("Запускаем радио")
                } else logMessage("Радио уже играет!")
            }
            else -> logMessage("Сначала нужно запустить двигатель")
        }
    }

    fun turnOff() {
        if (isTurnedOn) {
            !isTurnedOn
            logMessage("Дальше без музыки(")
        } else logMessage("Вообще-то, радио и так выключено")
    }

    fun autoSelectRadioChannel() {
        currentChannel = Random.nextDouble(100.0, 123.0)
        logMessage("Сейчас играет радиостанция: $currentChannel fm")
    }

    fun nextChannel() {
        currentChannel += if (currentChannel == 123.0) 100.0 else currentChannel++
        logMessage("Канал переключен, теперь играет: $currentChannel fm")
    }
}