package com.example.daggersample.objects.car

import com.example.daggersample.interfaces.Car
import com.example.daggersample.interfaces.Engine
import com.example.daggersample.interfaces.Tank
import com.example.daggersample.objects.audio_system.AudioSystem

/**
 * Реализация упрощенной машины, представленная машиной на бензине
 */
class PetrolCar(
    private val audioSystem: AudioSystem,
    private val engine: Engine,
    private val tank: Tank
) : Car {

    override fun launchEngine() {
        when {
            !needToRefuel() -> {
                engine.launch().apply {
                    if (this) tank.reduceFuel()
                }
            }
            else -> refuel()
        }
    }

    override fun switchOffEngine() {
        engine.switchOff()
    }

    override fun increaseSpeed() {
        engine.increaseGas()
    }

    override fun decreaseSpeed() {
        engine.increaseGas()
    }

    override fun refuel() {
        tank.fill()
    }

    override fun needToRefuel(): Boolean {
        return tank.isEmpty()
    }

    override fun turnOnAudioSystem() {
        audioSystem.apply {
            turnOn(engine.isLaunched())
            autoSelectRadioChannel()
        }
    }

    override fun turnOffAudioSystem() {
        audioSystem.turnOff()
    }

    override fun changeRadioStation() {
        audioSystem.nextChannel()
    }

    override fun autoSelectRadioChannel() {
        audioSystem.autoSelectRadioChannel()
    }

}