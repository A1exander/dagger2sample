package com.example.daggersample.objects.engine

import com.example.daggersample.interfaces.Engine
import com.example.daggersample.utils.logMessage

/**
 * Реализация двигателя для бензиновой машины, создаваемого даггером
 */
class PetrolEngine : Engine {

    private var engineLaunched: Boolean = false
    private var currentSpeed = 0.0

    override fun launch(): Boolean {
        return if (engineLaunched) {
            logMessage("Бензиновый двигатель уже заведен")
            false
        } else {
            logMessage("Запускаем двигатель, работающий на бензине")
            true
        }
    }

    override fun increaseGas() {
        currentSpeed += 10
        logMessage("Вот это мы летим со скоростью: $currentSpeed")
    }

    override fun decreaseGas() {
        currentSpeed = if (currentSpeed <= 10) 0.0 else currentSpeed - 10
        logMessage("Уффф, так быстро летели, хорошо, что притормаживаем")
    }

    override fun switchOff(): Boolean {
        return if (!engineLaunched) {
            logMessage("Двигатель на бензине уже заглушен")
            false
        } else {
            logMessage("Выключаем бензиновый двигатель")
            true
        }
    }

    override fun isLaunched(): Boolean = engineLaunched

}