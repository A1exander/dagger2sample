package com.example.daggersample.objects.qualifier_object

import com.example.daggersample.interfaces.QualifiedObject
import com.example.daggersample.utils.logMessage
import javax.inject.Inject

/**
 * Реализация объекта, определяемого квалифаером
 */
class FirstQualifierObject @Inject constructor() : QualifiedObject {

    override fun someMethodHere() {
        logMessage("Вызов какого-то метода в первом qualified-объекте")
    }

}