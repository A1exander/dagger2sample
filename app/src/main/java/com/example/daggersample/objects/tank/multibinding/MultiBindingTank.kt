package com.example.daggersample.objects.tank.multibinding

import com.example.daggersample.enums.TankStatus
import com.example.daggersample.interfaces.Tank
import com.example.daggersample.utils.logMessage
import javax.inject.Inject

/**
 * Пример объекта, используемого для мультибайндинга, в сет
 */
class MultiBindingTank @Inject constructor() : Tank {

    override fun isEmpty(): Boolean {
        logMessage("Проверка пустой ли бак в первом тестовом объекте")

        return false
    }

    override fun fill() {
        logMessage("Зполнение бака в первом тестовом объекте")
    }

    override fun status(): TankStatus {
        logMessage("Проверка статуса в первом тестовом объекте")

        return TankStatus.FULL
    }

    override fun reduceFuel() {
        logMessage("Уменьшаем топливо в баке первого тестового объекта")
    }
}