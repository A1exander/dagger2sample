package com.example.daggersample.objects.scoped_objects

import kotlin.random.Random

/**
 * Пример длинноживущего объекта(время жизни управляется аннотацией скоупа)
 */
class LongLiveObject {

    var variable = Random.nextInt()

}