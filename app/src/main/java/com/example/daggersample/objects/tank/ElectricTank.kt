package com.example.daggersample.objects.tank

import com.example.daggersample.enums.TankStatus
import com.example.daggersample.interfaces.Tank
import com.example.daggersample.utils.logMessage
import javax.inject.Inject

/**
 * Реализация бака для электрокара, создаваемого даггером
 */
class ElectricTank @Inject constructor() : Tank {

    private var tankStatus: TankStatus = TankStatus.FULL

    override fun isEmpty(): Boolean {
        logMessage(
            "А что, заряда нет?\n" +
                if (tankStatus == TankStatus.EMPTY) "Ага, пусто((" else "Да не, еще норм"
        )
        return tankStatus == TankStatus.EMPTY
    }

    override fun fill() {
        logMessage(
            "Сейчас аккум зарядим на макс!\n" +
                "Вот и зарядили)"
        )
        tankStatus = TankStatus.FULL
    }

    override fun status(): TankStatus {
        logMessage("А что по заряду слышно?\nЗаряда: $tankStatus")
        return tankStatus
    }

    override fun reduceFuel() {
        logMessage("Ща как покатаемся")
        tankStatus = when (tankStatus) {
            TankStatus.FULL -> TankStatus.HALF_FILLED
            TankStatus.HALF_FILLED -> TankStatus.EMPTY
            TankStatus.EMPTY -> throw Exception("Уппс, а зарядка-то кончилась")
        }
    }

}