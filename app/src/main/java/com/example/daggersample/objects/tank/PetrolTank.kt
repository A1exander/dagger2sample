package com.example.daggersample.objects.tank

import com.example.daggersample.enums.TankStatus
import com.example.daggersample.interfaces.Tank
import com.example.daggersample.utils.logMessage

/**
 * Реализация бака для бензиновой машины
 */
class PetrolTank : Tank {

    private var tankStatus: TankStatus = TankStatus.FULL

    override fun isEmpty(): Boolean {
        logMessage(
            "А что, бензин кончился?\n" +
                if (tankStatus == TankStatus.EMPTY) "Ага, пусто((" else "Да не, еще норм"
        )
        return tankStatus == TankStatus.EMPTY
    }

    override fun fill() {
        logMessage(
            "Сейчас как зальем бенза!\n" +
                "Бак полон, можем ехать"
        )
        tankStatus = TankStatus.FULL
    }

    override fun status(): TankStatus {
        logMessage("А как там с топливом?\nСостояние бака: $tankStatus")
        return tankStatus
    }

    override fun reduceFuel() {
        logMessage("Ща как покатаемся")
        tankStatus = when (tankStatus) {
            TankStatus.FULL -> TankStatus.HALF_FILLED
            TankStatus.HALF_FILLED -> TankStatus.EMPTY
            TankStatus.EMPTY -> throw Exception("Уппс, а бензика и нет совсем((")
        }
    }

}