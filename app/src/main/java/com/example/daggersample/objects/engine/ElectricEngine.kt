package com.example.daggersample.objects.engine

import com.example.daggersample.interfaces.Engine
import com.example.daggersample.utils.logMessage
import javax.inject.Inject

/**
 * Реализация двигателя для электрокара, создаваемого даггером
 * следует глянуть еще [com.example.daggersample.objects.engine.ElectricEngine]
 */
class ElectricEngine @Inject constructor() : Engine {

    private var engineLaunched: Boolean = false
    private var currentSpeed = 0.0

    override fun launch(): Boolean {
        return if (engineLaunched) {
            logMessage("Электродвигатель и так работает уже)")
            false
        } else {
            logMessage("Электродвигатель запускается")
            true
        }
    }

    override fun increaseGas() {
        currentSpeed += 10
        logMessage("Вот это мы летим со скоростью: $currentSpeed")
    }

    override fun decreaseGas() {
        currentSpeed = if (currentSpeed <= 10) 0.0 else currentSpeed - 10
        logMessage("Уффф, так быстро летели, хорошо, что притормаживаем")
    }

    override fun switchOff(): Boolean {
        return if (!engineLaunched) {
            logMessage("Электродвигатель уже заглушен")
            false
        } else {
            logMessage("Выключаем электродвигатель")
            true
        }
    }

    override fun isLaunched(): Boolean = engineLaunched

}