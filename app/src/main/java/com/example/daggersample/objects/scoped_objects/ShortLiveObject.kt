package com.example.daggersample.objects.scoped_objects

import java.util.*

/**
 * Пример короткоживущего объекта(время жизни управляется аннотацией скоупа)
 */
class ShortLiveObject {

    var variable = UUID.randomUUID().toString()

}