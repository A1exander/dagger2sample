package com.example.daggersample.objects.car

import com.example.daggersample.interfaces.Car
import com.example.daggersample.interfaces.Engine
import com.example.daggersample.interfaces.Tank
import com.example.daggersample.objects.audio_system.AudioSystem
import javax.inject.Inject

/**
 * Реализация упрощенной машины, представленная электрокаром
 *
 * Следует различать @Inject в поле и @Inject в конструкторе
 * Указывая эту аннотацию на конструктор, подразумевается, что создавать объект этого класса, будет
 * даггер, создание руками не требуется
 *
 * При таком использовании, даггер должен иметь возможность собственноручно создать все объекты, необходимые
 * для создания данного объекта
 */
class ElectricCar @Inject constructor(
    private val audioSystem: AudioSystem,
    private val tank: Tank,
    private val engine: Engine
) : Car {

    override fun launchEngine() {
        when {
            !needToRefuel() -> {
                engine.launch().apply {
                    if (this) tank.reduceFuel()
                }
            }
            else -> refuel()
        }
    }

    override fun switchOffEngine() {
        engine.switchOff()
    }

    override fun increaseSpeed() {
        engine.increaseGas()
    }

    override fun decreaseSpeed() {
        engine.increaseGas()
    }

    override fun refuel() {
        tank.fill()
    }

    override fun needToRefuel(): Boolean {
        return tank.isEmpty()
    }

    override fun turnOnAudioSystem() {
        audioSystem.apply {
            turnOn(engine.isLaunched())
            autoSelectRadioChannel()
        }
    }

    override fun turnOffAudioSystem() {
        audioSystem.turnOff()
    }

    override fun changeRadioStation() {
        audioSystem.nextChannel()
    }

    override fun autoSelectRadioChannel() {
        audioSystem.autoSelectRadioChannel()
    }

}