package com.example.daggersample.objects.tank.multibinding

import com.example.daggersample.enums.TankStatus
import com.example.daggersample.interfaces.Tank
import com.example.daggersample.utils.logMessage
import javax.inject.Inject

/**
 * Пример объекта, используемого для мультибайндинга, в сет
 */
class MultiBindingTank2 @Inject constructor() : Tank {

    override fun isEmpty(): Boolean {
        logMessage("Проверка пустой ли бак во втором тестовом объекте")

        return false
    }

    override fun fill() {
        logMessage("Зполнение бака во втором тестовом объекте")
    }

    override fun status(): TankStatus {
        logMessage("Проверка статуса во втором тестовом объекте")

        return TankStatus.FULL
    }

    override fun reduceFuel() {
        logMessage("Уменьшаем топливо в баке второго тестового объекта")
    }
}