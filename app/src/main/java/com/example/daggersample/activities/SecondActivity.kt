package com.example.daggersample.activities

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.daggersample.R
import com.example.daggersample.SampleApplication
import com.example.daggersample.di.components.DaggerSecondActivityComponent
import com.example.daggersample.di.modules.LongLiveObjectModule
import com.example.daggersample.di.modules.ShortLiveObjectModule
import com.example.daggersample.objects.scoped_objects.LongLiveObject
import com.example.daggersample.objects.scoped_objects.ShortLiveObject
import javax.inject.Inject

class SecondActivity : AppCompatActivity() {

    /**
     * Пример обычного инжекта
     * Для сравнения того, какие объекты поставляются в эту активити и во вторую, можно поставить брейкпоинт
     * и посмотреть id ссылки на объект
     *
     * P.S. Спойлер - shortLiveObject будет другой
     */
    @Inject
    lateinit var longLiveObject: LongLiveObject

    /**
     * Аналогично описанию longLiveObject
     */
    @Inject
    lateinit var shortLiveObject: ShortLiveObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /**
         * Создаем инстанс компонента, отвечающего за работу в второй активити
         */
        DaggerSecondActivityComponent
            .factory()
            /** Поставляем данному компоненту инстанс главного компонента, для того, чтобы были доступны
             * для использования зависимостей [LongLiveObjectModule] и [ShortLiveObjectModule]
             */
            .create(SampleApplication.mainComponent)
            // передаем ссылку на текущий объект активити, чтобы даггер смог инжектнуть все необходимые зависимости
            .inject(this)

        initViews()
    }

    private fun initViews() {
        setContentView(R.layout.activity_layout)

        findViewById<TextView>(R.id.title).apply {
            setText(R.string.second_activity_title)
        }

        findViewById<Button>(R.id.navigate_to_new_act).apply {
            visibility = View.INVISIBLE
        }
    }

}