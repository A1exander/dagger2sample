package com.example.daggersample.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.daggersample.R
import com.example.daggersample.SampleApplication
import com.example.daggersample.di.components.DaggerFirstActivityComponent
import com.example.daggersample.di.modules.LongLiveObjectModule
import com.example.daggersample.di.modules.ShortLiveObjectModule
import com.example.daggersample.di.qualifiers.FirstQualifiedObject
import com.example.daggersample.di.qualifiers.SecondQualifiedObject
import com.example.daggersample.interfaces.Car
import com.example.daggersample.interfaces.QualifiedObject
import com.example.daggersample.interfaces.Tank
import com.example.daggersample.objects.scoped_objects.LongLiveObject
import com.example.daggersample.objects.scoped_objects.ShortLiveObject
import javax.inject.Inject
import javax.inject.Named

class FirstActivity : AppCompatActivity() {

    /**
     * Пример инжекта именованного объекта, подробнее в [com.example.daggersample.di.modules.PetrolModule] и
     * [FirstQualifiedObject]
     */
    @Inject
    @Named("petrol")
    lateinit var petrolCar: Car

    /**
     * Пример обычного инжекта объекта, даггер понимает, что требуется неименованный объект
     * и без проблем его поставляет
     */
    @Inject
    lateinit var electricCar: Car

    /**
     * Пример инжекта объекта с квалифаером, подробнее в [com.example.daggersample.di.modules.QualifiedObjectsModule] и
     * [FirstQualifiedObject]
     */
    @Inject
    @FirstQualifiedObject
    lateinit var firstQualifierObject: QualifiedObject

    /**
     * Пример инжекта объекта с квалифаером, подробнее в [com.example.daggersample.di.modules.QualifiedObjectsModule] и
     * [SecondQualifiedObject]
     */
    @Inject
    @SecondQualifiedObject
    lateinit var secondQualifierObject: QualifiedObject

    /**
     * Пример инжекта сета(мультибайндинг)
     */
    @Inject
    lateinit var tanks: Set<@JvmSuppressWildcards Tank>

    /**
     * Пример обычного инжекта
     * Для сравнения того, какие объекты поставляются в эту активити и во вторую, можно поставить брейкпоинт
     * и посмотреть id ссылки на объект
     *
     * P.S. Спойлер - longLiveObject, будет такой же
     */
    @Inject
    lateinit var longLiveObject: LongLiveObject

    /**
     * Аналогично описанию longLiveObject
     */
    @Inject
    lateinit var shortLiveObject: ShortLiveObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /**
         * Создаем инстанс компонента, отвечающего за работу в первой активити
         */
        DaggerFirstActivityComponent
            .factory()
            /** Поставляем данному компоненту инстанс главного компонента, для того, чтобы были доступны
             * для использования зависимостей [LongLiveObjectModule] и [ShortLiveObjectModule]
             */
            .create(SampleApplication.mainComponent)
            // передаем ссылку на текущий объект активити, чтобы даггер смог инжектнуть все необходимые зависимости
            .inject(this)

        initViews()
    }

    private fun initViews() {
        setContentView(R.layout.activity_layout)

        findViewById<TextView>(R.id.title).apply {
            setText(R.string.first_activity_title)
        }

        findViewById<Button>(R.id.navigate_to_new_act).apply {
            setText(R.string.button_title)
            setOnClickListener {
                startActivity(
                    Intent(this@FirstActivity, SecondActivity::class.java)
                )
            }
        }
    }

}