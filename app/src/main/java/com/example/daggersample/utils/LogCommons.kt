package com.example.daggersample.utils

fun logMessage(message: String) {
    println("$message\n")
}