package com.example.daggersample.di.components

import com.example.daggersample.activities.FirstActivity
import com.example.daggersample.di.modules.ElectricModule
import com.example.daggersample.di.modules.MultibindingModule
import com.example.daggersample.di.modules.PetrolModule
import com.example.daggersample.di.modules.QualifiedObjectsModule
import com.example.daggersample.di.scopes.FirstActivityScope
import com.example.daggersample.objects.scoped_objects.LongLiveObject
import com.example.daggersample.objects.scoped_objects.ShortLiveObject
import dagger.Component
import dagger.Component.Factory

/**
 * Подробнее по описанию компонента - [MainComponent]
 *
 * Компонент, отвечающий за работу с объектами в первой активити
 *
 * В данном компоненте, присутствует параметр
 * @param dependencies - он указывает на компонент, от которого зависит текущий компонент
 * В данном случае, компонент зависит от [MainComponent], потому что в нем требуются
 * зависимости [ShortLiveObject] и [LongLiveObject]. Зависимость от другого компонента, надо явно указать при создании
 * объекта компонента. В данном случае, через фабричный метод create() мы получим инстанс [MainComponent]
 */
@Component(
    modules = [
        ElectricModule::class,
        PetrolModule::class,
        MultibindingModule::class,
        QualifiedObjectsModule::class
    ],
    dependencies = [MainComponent::class]
)
@FirstActivityScope
interface FirstActivityComponent {

    /**
     * Для того, чтобы даггер мог поставить инстанс в соответствующую переменную, необходимо дать доступ к
     * классу, в который инжектятся зависимости(в данном случае, к активити)
     *
     * Под капотом, даггер сгенерирует метод, с такой же сигнатурой и используя его, получит сслыку на активити,
     * после, обычным образом activityLink.dependencyVariable = correspondingInstance поставит зависимость
     *
     * Даггер определяет, куда ему что инжектить посредствам работы AnnotationProcessor/Kapt, а далее, все
     * названия переменных, он подставляет в свой шаблон генерации классов
     */
    fun inject(activity: FirstActivity)

    @Factory
    interface FirstActivityComponentFactory {
        /**
         * Тут нужно явно указать, что требуется инстанс [MainComponent], иначе даггер будет ругаться
         */
        fun create(mainComponent: MainComponent): FirstActivityComponent
    }
}