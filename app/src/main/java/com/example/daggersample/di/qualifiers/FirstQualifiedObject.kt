package com.example.daggersample.di.qualifiers

import javax.inject.Qualifier

/**
 * Создание аннотации, для помощи даггеру в различии объектов одного типа
 * Пример использования показан в [com.example.daggersample.activities.FirstActivity]
 *
 * Имеет аналог - @Named("some_string_qualifier")
 */
@Qualifier
annotation class FirstQualifiedObject
