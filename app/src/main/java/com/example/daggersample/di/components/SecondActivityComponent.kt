package com.example.daggersample.di.components

import com.example.daggersample.activities.SecondActivity
import com.example.daggersample.di.scopes.SecondActivityScope
import dagger.Component
import dagger.Component.Factory

/**
 * Компонент схож с [FirstActivityComponent]
 *
 * Компонент, отвечающий за работу с объектами во второй активити
 */
@Component(dependencies = [MainComponent::class])
@SecondActivityScope
interface SecondActivityComponent {

    fun inject(secondActivity: SecondActivity)

    @Factory
    interface SecondActivityComponentFactory {
        fun create(mainComponent: MainComponent): SecondActivityComponent
    }

}