package com.example.daggersample.di.modules

import com.example.daggersample.di.qualifiers.FirstQualifiedObject
import com.example.daggersample.di.qualifiers.SecondQualifiedObject
import com.example.daggersample.interfaces.QualifiedObject
import com.example.daggersample.objects.qualifier_object.FirstQualifierObject
import com.example.daggersample.objects.qualifier_object.SecondQualifierObject
import dagger.Binds
import dagger.Module

/**
 * Подробное описание модулей - [LongLiveObjectModule]
 *
 * Данный модуль показывает оборачивание конкретных объектов в абстракцию, использую при этом
 * созданные квалифаеры
 */
@Module
interface QualifiedObjectsModule {

    /**
     * Аннотация, для обертки в абстракцию
     * Мы показываем даггеру, что хотим получать вместо конкретной реализации [FirstQualifierObject] -> абстракцию [QualifiedObject]
     */
    @Binds
    /**
     * Используем квалифаер для того, чтобы даггер не запутался в объектах
     */
    @FirstQualifiedObject
    fun bindsFirstQualifiedObject(firstQualifierObject: FirstQualifierObject): QualifiedObject

    /** Аналогично описанному выше, с использованием другого квалифаера
     * [SecondQualifierObject] -> абстракцию [QualifiedObject]
     */
    @Binds
    @SecondQualifiedObject
    fun bindsSecondQualifiedObject(secondQualifierObject: SecondQualifierObject): QualifiedObject

}