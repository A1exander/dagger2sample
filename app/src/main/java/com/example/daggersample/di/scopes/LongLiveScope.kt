package com.example.daggersample.di.scopes

import javax.inject.Scope

/**
 * Скоуп для корневого компонента приложения - будет жить дольше всех
 */
@Scope
annotation class LongLiveScope
