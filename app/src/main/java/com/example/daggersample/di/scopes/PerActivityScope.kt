package com.example.daggersample.di.scopes

import javax.inject.Scope

/**
 * Скоуп(пример), как замена для [FirstActivityScope] и [SecondActivityScope]
 * Даггер позволяет использовать одинаковые скоупы, для отдельных компонентов, при этом не будет
 * коллизий между компонентами
 *
 * Исключением явлется компонент Родитель, по отношению к дочерним компонентам, даггер будет ругаться на использование
 * одинаковых скоупов - depends on scoped components in a non-hierarchical scope ordering
 *
 * Скойпы, доступные из коробки - @Singleton, @RootViewPickerScope, @Reusable
 */
@Scope
annotation class PerActivityScope
