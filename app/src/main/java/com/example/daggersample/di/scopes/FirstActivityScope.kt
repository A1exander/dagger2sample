package com.example.daggersample.di.scopes

import javax.inject.Scope

/**
 * Скоуп для компонента первой активити
 *
 * Чтобы не плодить скоупы, можно воспользоваться - @PerActivity [PerActivityScope]
 * Либо переиспользовать этот же скоуп(только название не подходит)
 */
@Scope
annotation class FirstActivityScope
