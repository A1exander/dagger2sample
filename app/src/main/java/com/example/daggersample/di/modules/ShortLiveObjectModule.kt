package com.example.daggersample.di.modules

import com.example.daggersample.objects.scoped_objects.ShortLiveObject
import dagger.Module
import dagger.Provides

/**
 * Подробное описание модулей - [LongLiveObjectModule]
 *
 * Данный модуль описывает создание "короткоживущего" объекта
 */
@Module
class ShortLiveObjectModule {

    /**
     * Указываем даггеру, что мы сами создадим объект
     *
     * Для данного объекта нам не требуется никаких параметров, поэтому метод ничего не принимает
     * Но возвращает созданный объект
     */
    @Provides
        /**
         * Тут отсутствуют какие-либо скоупы, соответственно, при каждом инжекте такого объекта, мы будем получать
         * новый инстанс
         */
    fun providesShortLiveObject(): ShortLiveObject {
        return ShortLiveObject()
    }

}