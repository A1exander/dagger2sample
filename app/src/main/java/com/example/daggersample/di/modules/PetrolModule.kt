package com.example.daggersample.di.modules

import com.example.daggersample.interfaces.Car
import com.example.daggersample.objects.audio_system.AudioSystem
import com.example.daggersample.objects.car.PetrolCar
import com.example.daggersample.objects.engine.PetrolEngine
import com.example.daggersample.objects.tank.PetrolTank
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Подробное описание модулей - [LongLiveObjectModule]
 *
 * В данном модуле приведен пример использования аннотации @Provides
 * Все объекты создаются ручками
 */
@Module
class PetrolModule {

    /**
     * Указываем даггеру, что мы сами создадим объект
     *
     * Для данного объекта нам не требуется никаких параметров, поэтому метод ничего не принимает
     * Но возвращает созданный объект
     */
    @Provides
        /**
         * Тут отсутствуют какие-либо скоупы, соответственно, при каждом инжекте такого объекта, мы будем получать
         * новый инстанс
         */
    fun providePetrolTank(): PetrolTank {
        return PetrolTank()
    }

    /**
     * Аналогично описанному выше
     */
    @Provides
    fun providePetrolEngine(): PetrolEngine {
        return PetrolEngine()
    }

    /**
     * Для создания [PetrolCar] нам требуются параметры, описываем их, как входные параметры метода
     * При инжекте [PetrolCar], даггер сам подставит сюда нужные объекты, т.к. мы описали создание объектов
     * [PetrolTank] и [PetrolEngine] выше, а объект [AudioSystem], будет автоматически создан даггером,
     * следовательно, все зависимости удовлетворены
     */
    @Provides
    fun providesPetrolCar(
        audioSystem: AudioSystem,
        engine: PetrolEngine,
        tank: PetrolTank
    ): PetrolCar {
        return PetrolCar(
            audioSystem = audioSystem,
            engine = engine,
            tank = tank
        )
    }

    /**
     * Тут указан пример оборачивание конкретной реализации в тип интерфейса, только с помощью аннотации @Provides
     * Метод выглядит ровно также, как все методы помеченные данной аннотацией, только взвращаемый тип объекта, уже
     * является интерфейсов
     */
    @Provides
    /**
     * Данная аннотация используется для того, чтобы даггер смог различать объекты одного и того же типа
     * Т.к. у нас в проекте используется еще один тип машины(электрокар) - [ElectricModule]#bindsElectricCar,
     * обернутый в абстракцию [Car], даггер не может определить, какой объект ему все таки нужно
     * поставить в [com.example.daggersample.activities.FirstActivity]
     *
     * Существует аналог данной аннотации(вернее, аннотация, на базе которой сделана эта) - @Qualifier,
     * подробнее про него - [com.example.daggersample.di.qualifiers.FirstQualifiedObject] и [QualifiedObjectsModule]
     */
    @Named("petrol")
    fun providesAbstractPetrolCar(petrolCar: PetrolCar): Car {
        return petrolCar
    }
}