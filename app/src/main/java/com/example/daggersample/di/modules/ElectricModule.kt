package com.example.daggersample.di.modules

import com.example.daggersample.interfaces.Car
import com.example.daggersample.interfaces.Engine
import com.example.daggersample.interfaces.Tank
import com.example.daggersample.objects.car.ElectricCar
import com.example.daggersample.objects.engine.ElectricEngine
import com.example.daggersample.objects.tank.ElectricTank
import dagger.Binds
import dagger.Module

/**
 * Подробное описание модулей - [LongLiveObjectModule]
 *
 * В данном модуле приведен пример использования аннотации @Binds
 * Мы не создаем объекты руками, а просто оборачиваем их в абстракцию
 * Тут используются все объекты, соответствующие "Электрокару"
 *
 * Объекты, получаемые методами в параметрах - создаются даггером(но для использования данной аннотации это не имеет роли,
 * можно так же создавать объекты руками)
 */
@Module
interface ElectricModule {

    /**
     * Аннотация, для обертки в абстракцию
     * Мы показываем даггеру, что хотим получать вместо конкретной реализации [ElectricEngine] -> абстракцию [Engine]
     */
    @Binds
        /**
         * Тут отсутствуют какие-либо скоупы, соответственно, при каждом инжекте такого объекта, мы будем получать
         * новый инстанс
         */
    fun bindsElectricEngine(electricEngine: ElectricEngine): Engine

    /**
     * Аналогично описанному выше
     * Реализация [ElectricTank] -> абстракция [Tank]
     */
    @Binds
    fun bindsElectricTank(electricTank: ElectricTank): Tank

    /**
     * Аналогично описанному выше
     * Реализация [ElectricCar] -> абстракция [Car]
     */
    @Binds
    fun bindsElectricCar(electricCar: ElectricCar): Car

}