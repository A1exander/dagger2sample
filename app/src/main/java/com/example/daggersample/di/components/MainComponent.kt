package com.example.daggersample.di.components

import com.example.daggersample.di.modules.LongLiveObjectModule
import com.example.daggersample.di.modules.ShortLiveObjectModule
import com.example.daggersample.di.scopes.LongLiveScope
import com.example.daggersample.objects.scoped_objects.LongLiveObject
import com.example.daggersample.objects.scoped_objects.ShortLiveObject
import dagger.Component
import dagger.Component.Factory

/**
 * Компонент является посредником(связующим звеном) между модулями(подробнее в [LongLiveObjectModule]) и
 * местами, куда инжектятся зависимости(пример [com.example.daggersample.activities.FirstActivity])
 *
 * Под капотом даггер сгененирурет класс MainComponentImpl, который будет имплементировать данный интерфейс
 *
 * Данный компонент является главным компонентом приложения
 * Так же, он прокидывает зависимости в другие компоненты, соответственно, ссылка на него должна поставляться
 * другим компонентам, зависимым от [LongLiveObjectModule] и [ShortLiveObjectModule]
 *
 * P.S. Даггер основан на кодогенерации. Простыми словами - есть шаблон для создания класса(-ов) и в момент
 * билда проекта, даггер начинает создавать классы по заготовленным шаблонам, с учетом того, что
 * насоздает пользователь-программист. В дальнейшем, мы (иногда) не совсем явно работаем с классами созданными дагерром,
 * как с обычными классами из Android или библиотек. Одним из примеров работы со сгенерированными даггером классами,
 * является создание компонента(в [com.example.daggersample.activities.FirstActivity] - обращение DaggerFirstActivityComponent).
 * Подробнее можно глянуть AnnotationProcessing/Kapt
 */
@Component(modules = [LongLiveObjectModule::class, ShortLiveObjectModule::class])
/**
 * Указываем скоуп данного компонента, объекты, помеченные таким же скоупом, как и
 * компонент будут иметь время жизни, такое же, как и компонент, а так же, будут своего рода синглтонами
 * При инжекте зависимости без указанного скоупа в несколько классов, каждый инжект будет создавать новый объект
 * Инжект с указанным скоупом, будет поставлять один и тот же объект
 */
@LongLiveScope
interface MainComponent {

    /**
     * Данный компонент прокидывает объект ShortLiveObject нижестоящим компонентам, для этого
     * мы определяем метод с соответствующим возвращаемым типом(впоследствие, в нужный нижестоящий компонент
     * необходимо будет передать объект данного компонента, пример [FirstActivityComponent]#create)
     *
     * Такого же эффекта можно добиться используя SubComponent, но в таком случае даггер-граф становится сложнее
     */
    fun provideShortLiveObject(): ShortLiveObject

    /**
     * Аналогично описанию выше
     */
    fun provideLongLiveObject(): LongLiveObject

    /**
     * Способ создания объекта даггер-компонента
     * Т.к. при генерации создается приватный конструктор, необходимо указать, как создавать объект
     * Возможны 2 способа Factory и Builder, по умолчанию, если не указывать способ создания,
     * даггер будет генерировать как будто указан Builder
     *
     * Отдельные правила создания обоих способов можно прочитать в документации этих аннотаций
     */
    @Factory
    interface MainComponentFactory {
        fun create(): MainComponent
    }
}