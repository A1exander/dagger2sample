package com.example.daggersample.di.modules

import com.example.daggersample.interfaces.Tank
import com.example.daggersample.objects.tank.multibinding.MultiBindingTank
import com.example.daggersample.objects.tank.multibinding.MultiBindingTank2
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet

/**
 * Подробное описание модулей - [LongLiveObjectModule]
 *
 * Данный модуль описывает использование мультибайндинга - создание коллеции объектов с одинаковым типом
 * В результате оба объекта будут находиться в Set'е элементов, который мы можем инжектить, как обычный объект
 *
 * Существует аналог - @IntoMap - соответственное различие - хранение конкретного объекта под ключом,
 * такая реализация полезна при байндинге вьюмоделей
 *
 * Результирующий сет инжектится в [com.example.daggersample.activities.FirstActivity]
 */
@Module
interface MultibindingModule {

    /**
     * Аннотация, для обертки в абстракцию
     * Мы показываем даггеру, что хотим получать вместо конкретной реализации [MultiBindingTank] -> абстракцию [Tank]
     */
    @Binds
    /**
     * Показываем даггеру, что этот объект необходимо положить в сет, который можно инжектить как обычный объект
     * Тут же можно использовать квалифаеры, тогда даггер будет различать Set'ы элементов, в зависимости от
     * квалифаера
     */
    @IntoSet
    fun bindsFirstTestTank(firstTank: MultiBindingTank): Tank

    /**
     * Аналогично описанному выше
     */
    @Binds
    @IntoSet
    fun bindsSecondTestTank(secondTank: MultiBindingTank2): Tank
}